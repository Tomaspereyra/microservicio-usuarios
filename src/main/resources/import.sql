INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('tomas98','$2a$10$LIH7dNcYph4hGmEw1fEol.zXpBWiIcB9LVzVIFXWX6vhYGStTGWsO',1,'tomas','pereyra','pereyratomas18@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$iRy5DCHi5FnoJa46yldyMeeKrVEt6ex2gXpq6Uc8PzFm7EuVc2fAi',1, 'John', 'Doe','jhon.doe@bolsadeideas.com');
INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');
INSERT INTO `usuarios_roles` (usuario_id, rol_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, rol_id) VALUES (2, 2);
