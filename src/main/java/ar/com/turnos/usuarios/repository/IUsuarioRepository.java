package ar.com.turnos.usuarios.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import ar.com.turnos.commons.usuarios.app.model.Usuario;


@RepositoryRestResource(path="usuarios")
public interface IUsuarioRepository extends JpaRepository<Usuario,Integer>{
	
	@RestResource(path="usuario")
	public Usuario findByUsername(@Param("nombre") String username);
	
}
